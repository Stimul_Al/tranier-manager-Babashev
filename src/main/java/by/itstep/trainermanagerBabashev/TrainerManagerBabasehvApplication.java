package by.itstep.trainermanagerBabashev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrainerManagerBabasehvApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrainerManagerBabasehvApplication.class, args);
	}

}
