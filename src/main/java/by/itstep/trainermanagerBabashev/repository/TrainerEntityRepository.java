package by.itstep.trainermanagerBabashev.repository;

import by.itstep.trainermanagerBabashev.entity.CommentEntity;
import by.itstep.trainermanagerBabashev.entity.TrainerEntity;

import java.util.List;

public interface TrainerEntityRepository {

    TrainerEntity findById(int id);

    List<TrainerEntity> findByWorkExp(int age);

    List<TrainerEntity> findAll();

    TrainerEntity createAccTrainer(TrainerEntity trainer);

    TrainerEntity updateAccTrainer(TrainerEntity trainer);

    void deleteAccTrainer(TrainerEntity trainer);

}
