package by.itstep.trainermanagerBabashev.repository;

import by.itstep.trainermanagerBabashev.entity.TrainerEntity;

import java.util.List;

public class TrainerEntityRepositoryImplements implements TrainerEntityRepository{

    @Override
    public TrainerEntity findById(int id) {
        return null;
    }

    @Override
    public List<TrainerEntity> findByWorkExp(int age) {
        return null;
    }

    @Override
    public List<TrainerEntity> findAll() {
        return null;
    }

    @Override
    public TrainerEntity createAccTrainer(TrainerEntity trainer) {
        return null;
    }

    @Override
    public TrainerEntity updateAccTrainer(TrainerEntity trainer) {
        return null;
    }

    @Override
    public void deleteAccTrainer(TrainerEntity trainer) {

    }
}
