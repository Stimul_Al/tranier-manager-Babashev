package by.itstep.trainermanagerBabashev.repository;

import by.itstep.trainermanagerBabashev.entity.AdminsEntity;
import by.itstep.trainermanagerBabashev.entity.CommentEntity;
import by.itstep.trainermanagerBabashev.entity.Role;

import java.util.List;

public class AdminEntityRepositoryImplements implements AdminEntityRepository{

    @Override
    public AdminsEntity findById(int id) {
        return null;
    }

    @Override
    public List<AdminsEntity> findAll(AdminsEntity admin) {
        return null;
    }

    @Override
    public List<AdminsEntity> findByRole(Role role) {
        return null;
    }

    @Override
    public void publicationComment(CommentEntity comment) {

    }

    @Override
    public AdminsEntity createAccAdmin(AdminsEntity admin) {
        return null;
    }

    @Override
    public AdminsEntity updateAccAdmin(AdminsEntity admin) {
        return null;
    }

    @Override
    public void deleteAccAdmin(AdminsEntity admin) {

    }
}
