package by.itstep.trainermanagerBabashev.repository;

import by.itstep.trainermanagerBabashev.entity.CommentEntity;

public class CommentEntityRepositoryImplements implements CommentEntityRepository {

    @Override
    public void createComment(CommentEntity comment) {

    }

    @Override
    public void deleteComment(CommentEntity comment) {

    }

    @Override
    public void updateComment(CommentEntity comment) {

    }

    @Override
    public CommentEntity getComment(CommentEntity comment) {
        return null;
    }
}
