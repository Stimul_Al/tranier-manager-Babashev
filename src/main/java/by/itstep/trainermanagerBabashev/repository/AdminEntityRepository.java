package by.itstep.trainermanagerBabashev.repository;

import by.itstep.trainermanagerBabashev.entity.AdminsEntity;
import by.itstep.trainermanagerBabashev.entity.CommentEntity;
import by.itstep.trainermanagerBabashev.entity.Role;
import by.itstep.trainermanagerBabashev.entity.TrainerEntity;

import java.util.List;

public interface AdminEntityRepository{

    AdminsEntity findById(int id);

    List<AdminsEntity> findAll(AdminsEntity admin);

    List<AdminsEntity> findByRole(Role role);

    void publicationComment(CommentEntity comment);

    AdminsEntity createAccAdmin(AdminsEntity admin);

    AdminsEntity updateAccAdmin(AdminsEntity admin);

    void deleteAccAdmin(AdminsEntity admin);
}
