package by.itstep.trainermanagerBabashev.repository;

import by.itstep.trainermanagerBabashev.entity.CommentEntity;

public interface CommentEntityRepository {

    void createComment(CommentEntity comment);

    void deleteComment(CommentEntity comment);

    void updateComment(CommentEntity comment);

    CommentEntity getComment(CommentEntity comment);
}
