package by.itstep.trainermanagerBabashev.entity;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
public class AdminsEntity {

    private Long id;

    private String name;

    private String lastName;

    private String email;

    private String password; //для каждого свой, не отображается

    private Role role;

}
