package by.itstep.trainermanagerBabashev.entity;

public enum Role {
    ADMIN,
    SUPERUSER
}
