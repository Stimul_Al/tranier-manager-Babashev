package by.itstep.trainermanagerBabashev.entity;

import lombok.*;

import java.util.List;

@Data
@NoArgsConstructor
@Builder
public class TrainerEntity {

    private Long id;

    private String name;

    private String lastName;

    private int workExperience;

    private String achievements;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private List<CommentEntity> comments;

    private String imgUrl;

    private String email;

    private String password;

}
