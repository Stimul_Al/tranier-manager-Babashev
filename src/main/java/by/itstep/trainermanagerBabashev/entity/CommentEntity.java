package by.itstep.trainermanagerBabashev.entity;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
public class CommentEntity {

    private Long id;

    private String content;

    private int note;

    private boolean state; // true -> опубликова; false -> не опубликован (в зависимости от решения админа)

}
